//
//  WebViewerViewController.swift
//  BreezoDrive
//
//  Created by Perry Shalom on 08/01/2019.
//  Copyright © 2019 Perry Shalom. All rights reserved.
//

import WebKit

class WebViewerViewController: UIViewController {
    //static let MOBILE_TERMS_OF_USE_ADDRESS: String = "http://talkandshow.com/terms-of-use/"
    
    var urlToShow: URL?
    var originalFilePath: String?

    @IBOutlet weak var _webView: WKWebView?
    @IBOutlet weak var webView: UIWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        webView.delegate = self
//        webView.scalesPageToFit = true
        
        if let urlToShow = urlToShow {
            setWebViewUrl(with: urlToShow.absoluteString)
        }

        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.action, target: self, action: #selector(onSendButtonPressed))
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.title = urlToShow?.absoluteString ?? "<no address>"
    }

    @objc func onSendButtonPressed(sender: UIBarButtonItem) {
        guard let originalFilePath = originalFilePath else { return }

        DataManager.share(fileWithUrlString: originalFilePath)
    }

    func setWebViewUrl(with urlString: String) {
        if let url = URL(string: urlString) {
            let request = URLRequest(url: url)

            if urlString.starts(with: "file://") {
                // https://stackoverflow.com/questions/24882834/wkwebview-not-loading-local-files-under-ios-8
                _webView?.loadFileURL(url, allowingReadAccessTo: url)
            } else {
                _webView?.load(request)
            }

            webView.loadRequest(request)
        }
    }

    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebView.NavigationType) -> Bool {
        return true
    }

    @IBAction func cancelButtonPressed(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
}
