//
//  AQRecorderViewController.swift
//  Runner
//
//  Created by Perry Shalom on 30/12/2018.
//  Copyright © 2018 The Chromium Authors. All rights reserved.
//

import UIKit
import CoreLocation
import MapKit

class AQRecorderViewController: UIViewController, LocationHelperDelegate {
    lazy var locationHelper = LocationHelper.shared
    lazy var airQualities = [AirQualityData]()

    static let MAXIMUM_VISIBLE_ANNOTATIONS: Int = 7

    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var btnRecord: UIButton!
    @IBOutlet weak var btnClean: UIButton!
    @IBOutlet weak var btnViewHistory: UIButton!
    @IBOutlet weak var btnLocateUser: UIButton!
    @IBOutlet weak var chartsContainer: UIView!
    lazy var chartView: ChartContainer = {
        let chartView: ChartContainer = ChartContainer.instantiateFromNib()
        chartsContainer.addSubview(chartView)
        chartView.stretchToSuperViewEdges()
        return chartView
    }()
    @IBOutlet weak var additionalDataContainer: UIView!
    lazy var additionalDataView: DataTableView = {
        let additionalDataView: DataTableView = DataTableView.instantiateFromNib()
        additionalDataContainer.addSubview(additionalDataView)
        additionalDataView.stretchToSuperViewEdges()
        return additionalDataView
    }()

    lazy var markersThrottler: Throttler = Throttler()
    lazy var regionRadius: CLLocationDistance = 100
    

    lazy var mockMovement: ClosureTimer = {
        let initialMockMovementLatitude: CLLocationDegrees = 32.337398
        let initialMockMovementLongitude: CLLocationDegrees = 34.855633

        return ClosureTimer(afterDelay: 4, userInfo: nil, repeats: true) { [weak self] (tuple) in
            guard let strongSelf = self else { return }
            let timer = tuple.0
            //📗(timer.counter)
            let addition = Double(timer.counter) / 100
            let location: CLLocation = CLLocation(latitude: initialMockMovementLatitude + addition, longitude: initialMockMovementLongitude + addition)
            
            guard Configurations.shared.shouldMockLocationAndData else { return }

            self?.onLocationUpdated(updatedLocation: location)
        }
    }()

    var isRecording: Bool = false {
        didSet {
            if oldValue == false && isRecording == true {
                DataManager.shared.startSavingResponses()

                if Configurations.shared.shouldMockLocationAndData {
                    📗(mockMovement.block)
                } else {
                    locationHelper.startUpdate()
                    if let currentLocation = locationHelper.currentLocation {
                        onLocationUpdated(updatedLocation: currentLocation)
                    }
                }
            }

            if isRecording {
                btnViewHistory.isPresented = false
                btnClean.isPresented = false
                btnRecord.setImage(UIImage(named: "btn-stop"), for: UIControl.State.normal)
            } else {
                btnViewHistory.isPresented = true
                btnRecord.setImage(UIImage(named: "btn-record"), for: UIControl.State.normal)
                if airQualities.count > 0 {
                    MainNavigationController.shared.showLoader()
                    PerrFuncs.runOnBackground { [weak self] in
                        self?.airQualities.remove(where: { $0.timestamp == 0 } )
                        DataManager.shared.saveResponsesJson(completion: { (filePath) in
                            if let _ = filePath {
                                📗("saved last records")
                                PerrFuncs.runOnUiThread(block: {
                                    self?.configureUi()
                                    MainNavigationController.shared.hideLoader(completion: { _ in
                                        UIAlertController.makeAlert(title: "Done recording", message: "Would you like to see the results?")
                                            .withAction(UIAlertAction(title: "See results", style: UIAlertAction.Style.default, handler: { (action) in
                                                guard self?.airQualities.count ?? 0 > 0 else { return }
                                                
                                                let recordsHistoryViewController: HistoryViewController = HistoryViewController.instantiate()
                                                self?.navigationController?.pushViewController(recordsHistoryViewController, animated: true)
                                            }))
                                            .withAction(UIAlertAction(title: "Pass", style: UIAlertAction.Style.cancel, handler: nil))
                                            .show()
                                    })
                                })
                            } else {
                                📕("failed to save last records")
                                MainNavigationController.shared.hideLoader()
                            }
                        })
                    }
                }
            }
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        isRecording = false

        mapView.delegate = self
        locationHelper.delegate = self
        locationHelper.distanceFilter = 300

        btnViewHistory.onClick { [weak self] _ in
            self?.navigationController?.pushViewController(HistoryViewController.instantiate(), animated: true)
        }

        btnLocateUser.onClick { [weak self] _ in
            if LocationHelper.shared.isPermissionGranted {
                self?.mapView.userTrackingMode = .follow
            } else {
                self?.present(PermissionsViewController.instantiate(), animated: true, completion: nil)
            }
        }

//        chartView.footerView = chartFooterView
//        let lblHeader = UILabel(frame: MainNavigationController.shared.view.frame)
//        lblHeader.frame = lblHeader.frame.withHeight(height: 30)
//        lblHeader.textAlignment = .center
//        lblHeader.text = "BAQIs"
//        chartView.headerView = lblHeader

        btnClean.onClick { [weak self] _ in
            guard let strongSelf = self else { return }

            if strongSelf.airQualities.count > 0 {
                UIAlertController.makeActionSheet(title: "Reset?".localized(), message: "Remove previous air quality details?".localized())
                    .withAction(UIAlertAction(title: "Clear old details".localized(), style: UIAlertAction.Style.destructive, handler: { [weak self] action in
                        self?.airQualities.removeAll()
                        if let annotations = self?.mapView.annotations {
                            self?.mapView.removeAnnotations(annotations)
                        }
                        if let overlays = self?.mapView.overlays {
                            self?.mapView.removeOverlays(overlays)
                        }
                        self?.configureUi()
                    }))
                    .withAction(UIAlertAction(title: "Cancel".localized(), style: UIAlertAction.Style.cancel, handler: nil))
                    .show()
            }
            
            strongSelf.configureUi()
        }

        btnRecord.onClick { [weak self] onClickListener in
            guard let strongSelf = self else { return }

            if LocationHelper.shared.isPermissionGranted {
//                strongSelf.btnRecord.animateZoom(zoomIn: false, duration: 0.5, completion: { _ in
                    strongSelf.isRecording = !strongSelf.isRecording
//                    strongSelf.btnRecord.animateZoom(zoomIn: true, duration: 0.5, completion: { _ in
                        //
//                    })
//                })
            } else {
                strongSelf.present(PermissionsViewController.instantiate(), animated: true, completion: nil)
            }
        }
        
        configureTexts()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        configureUi()

        if locationHelper.isPermissionGranted {
            // My location
            if Configurations.shared.shouldShowUserLocation {
                mapView.userTrackingMode = .follow
            }
        }
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        chartView.refreshChart(withData: airQualities)
    }
    
    func configureTexts() {
        title = "Air Quality Recorder".localized()
        navigationController?.title = "Air Quality Recorder".localized()
    
        chartView.configureTexts()
    }
    
    func configureUi() {
        pageControl.isPresented = false

        btnClean.isPresented = airQualities.count > 0
        chartView.refreshChart(withData: airQualities)
        additionalDataView.isPresented = true
        btnViewHistory.makeRoundedCorners()
        btnViewHistory.backgroundColor = UIColor.blue
        btnViewHistory.setTitleColor(UIColor.white, for: UIControl.State.normal)
    }

    func onLocationUpdated(updatedLocation: CLLocation) {
        guard isRecording else { return }

        //TODO: check if the radius diff is significant
        let placeHolder = AirQualityData(timestamp: 0, locationCoordinate: updatedLocation.coordinate.toLocationCoordinate(), baqi: 0)
        let previous = airQualities.last
        airQualities.append(placeHolder)
        📗("updatedLocation: \(updatedLocation)")
        DataManager.fetchBaqi(latitude: updatedLocation.coordinate.latitude, longitude: updatedLocation.coordinate.longitude, completion: { [weak self] aqData in
            if let aqData = aqData {
                📗("Got BAQI: \(String(describing: aqData))")
                PerrFuncs.runOnUiThread(block: {
                    placeHolder.import(fromOther: aqData)
                    guard let strongSelf = self else { return }

                    if let previous = previous, previous.baqi == 0 {
                        // hmmm...
                    } else {
                        strongSelf.addToMap(aqData: aqData, after: previous)
                        strongSelf.mapView.showAnnotations(strongSelf.mapView.annotations, animated: true)
                        strongSelf.chartView.refreshChart(withData: strongSelf.airQualities)
                        strongSelf.additionalDataView.configure(withData: aqData)
                    }
                })
            } else {
                self?.airQualities.remove(where: { $0 == placeHolder })
                📕("Got BAQI: \(String(describing: aqData))")
            }
        })
    }
    
    func moveMapCamera(toLocation location: CLLocation) {
        let coordinateRegion = MKCoordinateRegion(center: location.coordinate, latitudinalMeters: regionRadius * 2.0, longitudinalMeters: regionRadius * 2.0)
        mapView.setRegion(coordinateRegion, animated: true)
    }

    func addToMap(aqData: AirQualityData, after previous: AirQualityData?) {
        let annotation = BaqiAnnotation(data: aqData)

        if let previous = previous {
            let polyline = GradientPolylineOverlay(qualities: [previous, aqData])
            self.mapView.addOverlay(polyline)
        }

        mapView.addAnnotation(annotation)
    }
}

extension CLLocationCoordinate2D {
    func toLocationCoordinate() -> LocationCoordinate {
        return LocationCoordinate(latitude: latitude, longitude: longitude)
    }
}

extension LocationCoordinate {
    func toLocationCoordinate2D() -> CLLocationCoordinate2D {
        return CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
    }
}

extension AQRecorderViewController: MKMapViewDelegate {

    // https://stackoverflow.com/questions/12198390/mkmapview-and-annotations-hiding-with-zoom
    func mapView(_ mapView: MKMapView, regionDidChangeAnimated animated: Bool) {
//        markersThrottler.throttle(timeout: 1) {
//            let visibleAnnotations = mapView.visibleAnnotations()
//            guard visibleAnnotations.count > AQRecorderViewController.MAXIMUM_VISIBLE_ANNOTATIONS else { return }
//
//            var annotations = visibleAnnotations
//            annotations.removeAll()
//            var removeEachCount = visibleAnnotations.count % AQRecorderViewController.MAXIMUM_VISIBLE_ANNOTATIONS
//            if removeEachCount == 0 {
//                removeEachCount += 1
//            }
//
//            visibleAnnotations.forEach( { mapView.view(for: $0)?.isPresented = true } )
//            for i in 1..<(visibleAnnotations.count - 1) {
//                if i % removeEachCount != 0 {
//                    mapView.view(for: visibleAnnotations[i])?.isPresented = false
//                }
//            }
//        }

//        for annotationWithView in visibleAnnotations {
//            guard !(annotationWithView is MKUserLocation) else { return }
//
//            if mapView.region.span.latitudeDelta < 1 {
//                mapView.view(for: annotationWithView)?.isPresented = true
//            } else {
//                mapView.view(for: annotationWithView)?.isPresented = false
//            }
//        }
    }

    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {

        let polyLineRender: MKOverlayRenderer = (overlay as? GradientPolylineOverlay)?.renderer(withLineWidth: 20) ?? MKOverlayRenderer()
        return polyLineRender
    }

    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        let annotationView: MKAnnotationView

        if let baqiAnnotation = annotation as? BaqiAnnotation {
            let baqiMarker: BaqiAnnotationView
            
            if let dequeuedAnnotationView = mapView.dequeueReusableAnnotationView(withIdentifier: BaqiAnnotationView.AnnotationIdentifier) {
                dequeuedAnnotationView.annotation = baqiAnnotation
                // Already instantiated
                annotationView = dequeuedAnnotationView
                let baqiMarkerView = annotationView.subviews.filter( { $0 is BaqiAnnotationView } )
                baqiMarker = baqiMarkerView.first as! BaqiAnnotationView
            } else {
                // First instantiation
                annotationView = MKAnnotationView(annotation: baqiAnnotation, reuseIdentifier: BaqiAnnotationView.AnnotationIdentifier)
                annotationView.canShowCallout = true
                baqiMarker = BaqiAnnotationView.instantiateFromNib()
                annotationView.addSubview(baqiMarker)
                baqiMarker.stretchToSuperViewEdges()
            }

            // Configure marker
            baqiAnnotation.title = annotation.title ?? ""
            baqiAnnotation.subtitle = annotation.subtitle ?? ""
            if let qaData = baqiAnnotation.data {
                annotationView.loadCustomLines(fromData: qaData)
            }

            baqiMarker.bgColor = UIColor(withBaqi: baqiAnnotation.baqiValue)
            baqiMarker.text = baqiAnnotation.stringValue
            //annotationView.tintColor = UIColor(withBaqi: baqiAnnotation.value)
            baqiMarker.isPresented = Configurations.shared.shouldShowBaqiMarkers ? true : false
        } else if let userLocationAnnotation = annotation as? MKUserLocation {
            if let dequeuedAnnotationView = mapView.dequeueReusableAnnotationView(withIdentifier: "UserLocation") {
                dequeuedAnnotationView.annotation = userLocationAnnotation
                // Already instantiated
                annotationView = dequeuedAnnotationView
            } else {
                // First instantiation
                annotationView = MKAnnotationView(annotation: userLocationAnnotation, reuseIdentifier: "UserLocation")
                let locationView: UIView
                if Configurations.shared.currentVersion == "honda" {
                    locationView = UIImageView(image: UIImage(named: "location-honda"))
                } else {
                    locationView = UIImageView(image: UIImage(named: "general-location"))
                }
                annotationView.addSubview(locationView)
                locationView.center = annotationView.center
                //locationHondaImageView.stretchToSuperViewEdges()
            }
            annotationView.canShowCallout = true
        } else {
            📕("Unhandled annotation view")
            annotationView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: "default annotation")
        }

        return annotationView
    }
}

//extension CLLocation {
//    convenience init(lat: CLLocationDegrees, lng: CLLocationDegrees) {
//        self.init(coordinate: CLLocationCoordinate2D(latitude: lat, longitude: lng), altitude: 0, horizontalAccuracy: 5, verticalAccuracy: 5, course: CLLocationDirection(), speed: 0, timestamp: Date())
//    }
//}

extension AQRecorderViewController: UITextViewDelegate {
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        return false
    }
}

class BaqiAnnotation: MKPointAnnotation {
    let baqiValue: Int
    let stringValue: String
    private(set) var data: AirQualityData?

    init(data: AirQualityData) {
        self.baqiValue = data.baqi
        self.stringValue = data.laqiDisplay ?? "\(data.baqi)"
        self.data = data
        super.init()

        title = "details:".localized()
        subtitle = data.uiDescription
        coordinate = data.locationCoordinate.toLocationCoordinate2D()
    }
}

extension MKMapView {
    func visibleAnnotations() -> [MKAnnotation] {
        return annotations.filter( { view(for: $0) != nil } )
    }
}

extension MKAnnotationView {
    
    func loadCustomLines(customLines: [String]) {
        let stackView = self.stackView()
        for line in customLines {
            let label = UILabel()
            label.text = line
            label.textColor = UIColor.red
            label.textAlignment = .center
            label.font = label.font.withSize(label.font.pointSize - 2)
            stackView.addArrangedSubview(label)
        }
        self.detailCalloutAccessoryView = stackView
    }
    
    func loadCustomLines(fromData data: AirQualityData) {
        var lines: [String] = []
        for line in data.uiDescription.split(separator: "\n") {
            lines.append(String(line))
        }
        loadCustomLines(customLines: lines)
    }
    
    private func stackView() -> UIStackView {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.distribution = .fillEqually
        stackView.alignment = .fill
        return stackView
    }
}
