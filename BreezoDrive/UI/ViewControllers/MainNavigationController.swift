//
//  MainNavigationController.swift
//  BreezoDrive
//
//  Created by Perry Shalom on 13/01/2019.
//  Copyright © 2019 Perry Shalom. All rights reserved.
//

import Foundation

class MainNavigationController: UINavigationController {
    static weak var shared: MainNavigationController!
    //weak var blurEffect: UIVisualEffectView?
    weak var fullscreenLoaderView: UIView?
    override func viewDidLoad() {
         super.viewDidLoad()
        MainNavigationController.shared = self
    }

    override func motionBegan(_ motion: UIEvent.EventSubtype, with event: UIEvent?) {
        if motion == .motionShake {
            FLEXManager.shared()?.showExplorer()
        }
    }
    
    func showLoader(andBlockUi shouldBlock: Bool = true) {
        let _fullscreenLoaderView: UIView = UIView(frame: view.frame)
        fullscreenLoaderView = _fullscreenLoaderView
        fullscreenLoaderView?.isPresented = false
        view.window?.addSubview(_fullscreenLoaderView)

        //fullscreenLoaderView?.backgroundColor = UIColor.appGray.withAlphaComponent(0.7)
        fullscreenLoaderView?.isUserInteractionEnabled = shouldBlock
        fullscreenLoaderView?.stretchToSuperViewEdges()
        let blurEffect: UIVisualEffectView = UIVisualEffectView(effect: UIBlurEffect(style: .dark))
        fullscreenLoaderView?.addSubview(blurEffect)
        blurEffect.stretchToSuperViewEdges()
        let activityIndicator = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.white)
        fullscreenLoaderView?.addSubview(activityIndicator)
        activityIndicator.pinToSuperViewCenter()
        activityIndicator.startAnimating()

        fullscreenLoaderView?.😘(huggedObject: Timestamp.current)
        
        fullscreenLoaderView?.animateFade(fadeIn: true)
    }

    func hideLoader(completion: (CallbackClosure<Bool>)? = nil) {
        let isPresentedLongEnough = Timestamp.current - (fullscreenLoaderView?.😍() as? Timestamp ?? 0) > 1000
        if !isPresentedLongEnough {
            PerrFuncs.runBlockAfterDelay(afterDelay: 1, block: { [weak self] in
                self?.fullscreenLoaderView?.animateFade(fadeIn: false, duration: 0.5, completion: { [weak self] isDone in
                    self?.fullscreenLoaderView?.removeFromSuperview()
                    completion?(isDone)
                })
            })
        } else {
            fullscreenLoaderView?.animateFade(fadeIn: false, duration: 0.5, completion: { [weak self] isDone in
                self?.fullscreenLoaderView?.removeFromSuperview()
                completion?(isDone)
            })
        }
    }
}
