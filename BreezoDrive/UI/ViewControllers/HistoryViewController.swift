//
//  HistoryViewController.swift
//  Runner
//
//  Created by Perry Shalom on 06/01/2019.
//  Copyright © 2019 The Chromium Authors. All rights reserved.
//

import UIKit
import CoreGraphics

class HistoryViewController: UIViewController {
    var data: [URL] {
        return DataManager.shared.savedRecordsList()
    }
    //var tableView: IndependentTableView<AirQualityData, AirQualityCell>!

    @IBOutlet weak var tableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()

        //tableView = IndependentTableView(frame: view.frame)
        //view.addSubview(tableView)
        //tableView.stretchToSuperViewEdges()
        //tableView.data = aqConditions

        tableView.delegate = self
        tableView.dataSource = self

//        view.onSwipe(direction: UISwipeGestureRecognizerDirection.down) { [weak self] _ in
//            self?.dismiss(animated: true, completion: nil)
//        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        title = "Records History".localized()
    }
}

//class AirQualityCell: UITableViewCell {
//    static let CellReuseIdentifier: String = "AirQualityCell"
//    func configure(item: AirQualityData) {
//        textLabel?.text = "\(item.baqi)"
//        detailTextLabel?.text = "\(item.locationCoordinate)"
//    }
//}

class RecordCell: UITableViewCell {
    static let CellReuseIdentifier: String = PerrFuncs.className(RecordCell.self)
    func configure(item: URL) {
        let substrings = item.lastPathComponent.split(separator: "_")
        guard substrings.count > 0, let timestampString = substrings.first, let timestamp = Timestamp(String(timestampString)) else { prepareForReuse(); return }
        //Timestamp.current.date

        let details = item.lastPathComponent.replacingOccurrences(of: timestampString, with: "").replacingOccurrences(of: "_", with: " ")
        textLabel?.text = timestamp.date.toString()
        detailTextLabel?.text = details
    }

    override func prepareForReuse() {
        super.prepareForReuse()

        textLabel?.text = nil
        detailTextLabel?.text = nil
    }
}

extension HistoryViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let fileUrl = data[indexPath.row]

        UIAlertController.makeActionSheet(title: "Details".localized(), message: fileUrl.lastPathComponent)
            .withAction(UIAlertAction(title: "Cancel".localized(), style: UIAlertAction.Style.cancel, handler: { _ in
                tableView.deselectRow(at: indexPath, animated: true)
            }))
//            .withAction(UIAlertAction(title: "Send".localized(), style: UIAlertAction.Style.default, handler: { _ in
//                DataManager.share(fileWithUrl: fileUrl)
//                tableView.deselectRow(at: indexPath, animated: true)
//            }))
            .withAction(UIAlertAction(title: "Delete file".localized(), style: UIAlertAction.Style.destructive, handler: { [weak self] _ in
                tableView.deselectRow(at: indexPath, animated: true)
                UIAlertController.makeActionSheet(title: "Are you sure?".localized(), message: "No way back from here".localized()).withAction(UIAlertAction(title: "Yes, delete!".localized(), style: UIAlertAction.Style.destructive, handler: { _ in
                    DataManager.deleteFile(atUrl: fileUrl)
                    self?.tableView.reloadData()
                }))
                    .withAction(UIAlertAction(title: "Oops".localized(), style: UIAlertAction.Style.cancel, handler: nil))
                    .show()
            }))
            .withAction(UIAlertAction(title: "View / Send JSON".localized(), style: UIAlertAction.Style.default, handler: { [weak self] _ in
                let webViewerViewController = WebViewerViewController.instantiate()
                let _jsonViewerHtml = DataManager.shared.prepareJsonViewer(forJsonFileWithPath: fileUrl)?.absoluteString.toUrlPath()
                tableView.deselectRow(at: indexPath, animated: true)
                guard let jsonViewerHtml = _jsonViewerHtml else {
                    📕("Failed to prepare JSON file! path: \(fileUrl)")
                    ToastMessage.show(messageText: "Failed to prepare JSON file! 😯")
                    return
                }
                webViewerViewController.originalFilePath = fileUrl.absoluteString
                webViewerViewController.urlToShow = jsonViewerHtml
                self?.navigationController?.pushViewController(webViewerViewController, animated: true)
            }))
            .show()
    }
}

extension HistoryViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        let cell = tableView.dequeueReusableCell(withIdentifier: AirQualityCell.CellReuseIdentifier, for: indexPath) as! AirQualityCell
        let cell = tableView.dequeueReusableCell(withIdentifier: RecordCell.CellReuseIdentifier, for: indexPath) as! RecordCell
        cell.configure(item: data[indexPath.row])
        
        return cell
    }
}

//func sizeof(type: Type) -> Int {
//    fatalError("Unimplemented method")
//    return 0//return MemoryLayout<type>.size
//}
