//
//  PermissionsViewController.swift
//  BreezoDrive
//
//  Created by Perry Shalom on 09/01/2019.
//  Copyright © 2019 Perry Shalom. All rights reserved.
//

import Foundation

class PermissionsViewController: UIViewController {

    @IBOutlet weak var lblLocationPermissionExplanation: UILabel!
    @IBOutlet weak var btnGrantPermission: UIButton!
    var permissionChecker: ClosureTimer?

    override func viewDidLoad() {
        super.viewDidLoad()

        lblLocationPermissionExplanation.text = "This app requires permission for device location in order work properly".localized()
        btnGrantPermission.setTitle("Grant Permission".localized(), for: UIControl.State.normal)
        btnGrantPermission.onClick { _ in
            LocationHelper.shared.requestPermissionsIfNeeded()
        }
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        NotificationCenter.default.addObserver(self, selector: #selector(applicationWillEnterForeground), name: UIApplication.willEnterForegroundNotification, object: nil)

        if LocationHelper.shared.isPermissionGranted {
            dismiss(animated: true, completion: nil)
        } else {
            permissionChecker?.cancel()
            permissionChecker = ClosureTimer(afterDelay: 1, userInfo: nil, repeats: true) { [weak self] (tuple) in
                guard let strongSelf = self, strongSelf.permissionChecker?.timer?.isValid ?? false else { return }
                if LocationHelper.shared.isPermissionGranted {
                    strongSelf.permissionChecker?.cancel()
                    strongSelf.dismiss(animated: true, completion: nil)
                }
            }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        NotificationCenter.default.removeObserver(self)
    }

    @objc func applicationWillEnterForeground(notification: Notification) {
        if LocationHelper.shared.isPermissionGranted {
            permissionChecker?.cancel()
            dismiss(animated: true, completion: nil)
        }
    }
}
