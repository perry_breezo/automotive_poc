//
//  ChartContainer.swift
//  BreezoDrive
//
//  Created by Perry Shalom on 14/01/2019.
//  Copyright © 2019 Perry Shalom. All rights reserved.
//

import Foundation

class DataTableView: UIView {

    @IBOutlet weak var lblPm25Value: UILabel!
    @IBOutlet weak var lblPm25Units: UILabel!

    @IBOutlet weak var lblPm10Value: UILabel!
    @IBOutlet weak var lblPm10Units: UILabel!

    @IBOutlet weak var lblO3Value: UILabel!
    @IBOutlet weak var lblO3Units: UILabel!

    @IBOutlet weak var lblCoValue: UILabel!
    @IBOutlet weak var lblCoUnits: UILabel!

    @IBOutlet weak var lblNo2Value: UILabel!
    @IBOutlet weak var lblNo2Units: UILabel!

    @IBOutlet weak var lblSo2Value: UILabel!
    @IBOutlet weak var lblSo2Units: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    func configure(withData data: AirQualityData) {
        lblPm25Value.text = data.pm25Value != nil ? String(describing: data.pm25Value ?? 0) : "??"
        lblPm25Units.text = data.pm25Units ?? "??"

        lblPm10Value.text = data.pm10Value != nil ? String(describing: data.pm10Value ?? 0) : "??"
        lblPm10Units.text = data.pm10Units ?? "??"

        lblO3Value.text = data.o3Value != nil ? String(describing: data.o3Value ?? 0) : "??"
        lblO3Units.text = data.o3Units ?? "??"

        lblCoValue.text = data.coValue != nil ? String(describing: data.coValue ?? 0) : "??"
        lblCoUnits.text = data.coUnits ?? "??"

        lblNo2Value.text = data.no2Value != nil ? String(describing: data.no2Value ?? 0) : "??"
        lblNo2Units.text = data.no2Units ?? "??"

        lblSo2Value.text = data.so2Value != nil ? String(describing: data.so2Value ?? 0) : "??"
        lblSo2Units.text = data.so2Units ?? "??"

        lblPm25Value.textColor = UIColor.black
        lblPm10Value.textColor = UIColor.black
        lblO3Value.textColor = UIColor.black
        lblCoValue.textColor = UIColor.black
        lblNo2Value.textColor = UIColor.black
        lblSo2Value.textColor = UIColor.black

        if let dominantPollutant = data.dominantPollutant {
            switch dominantPollutant {
            case "co":
                lblCoValue.textColor = UIColor.red
            case "no2":
                lblNo2Value.textColor = UIColor.red
            case "o3":
                lblO3Value.textColor = UIColor.red
            case "pm10":
                lblPm10Value.textColor = UIColor.red
            case "pm25":
                lblPm25Value.textColor = UIColor.red
            case "so2":
                lblSo2Value.textColor = UIColor.red
            default:
                break
            }
        }

    }

    func configureTexts() {
    }
}

