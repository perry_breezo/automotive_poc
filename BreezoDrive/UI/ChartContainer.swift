//
//  ChartContainer.swift
//  BreezoDrive
//
//  Created by Perry Shalom on 14/01/2019.
//  Copyright © 2019 Perry Shalom. All rights reserved.
//

import Foundation
import Charts
import CoreLocation

class ChartContainer: UIView {
    @IBOutlet weak var chartsContainer: UIView!
    @IBOutlet weak var lblWorseTitle: UILabel!
    @IBOutlet weak var lblBestTitle: UILabel!
    @IBOutlet weak var lblChartTitle: UILabel!
    @IBOutlet weak var lblChartExplanation: UILabel!

    lazy var chartView: BarChartView = {
        let chartView = BarChartView(frame: frame)
        chartsContainer.addSubview(chartView)
        chartView.stretchToSuperViewEdges()
        chartView.setScaleEnabled(false)
        
        return chartView
    }()

    func refreshChart(withData data: [AirQualityData]) {
        var entries = [BarChartDataEntry]()
        var colors = [UIColor]()
        let first: AirQualityData? = data.first
        for airQualityData in data {
            // https://stackoverflow.com/questions/1348604/distance-between-two-coordinates-with-corelocation
            let kmDistance: Int
            if let first = first {
                let bistanceBetween = Int(CLLocationDistance.between(coordinates: (airQualityData.locationCoordinate.toLocationCoordinate2D(), first.locationCoordinate.toLocationCoordinate2D())) / 1000)
                kmDistance = bistanceBetween < 0 ? 0 : bistanceBetween
            } else {
                kmDistance = 0
            }
            
            let entry = BarChartDataEntry(x: Double(kmDistance), y: Double(airQualityData.baqi))
            entries.append(entry)
            colors.append(UIColor(withBaqi: airQualityData.baqi))
        }
        
        let dataSet = BarChartDataSet(values: entries, label: "BAQIs")
        let data = BarChartData(dataSets: [dataSet])
        dataSet.colors = colors
        chartView.data = data
        
        chartView.notifyDataSetChanged()
    }
    
    func configureTexts() {
        lblBestTitle.text = "excellent".localized()
        lblWorseTitle.text = "poor".localized()
        lblChartTitle.text = "Air Quality Summary".localized()
        lblChartExplanation.text = "Presented per kilometer".localized()
    }
}

extension CLLocationDistance {
    /**
     Returns the distance (measured in meters) from one location's coordinate to another, regardless the altitude.
     
     - coordinates: A tuple of two coordinates
     */
    static func between(coordinates: (CLLocationCoordinate2D, CLLocationCoordinate2D)) -> CLLocationDistance {
        let location1 = CLLocation(latitude: coordinates.0.latitude, longitude: coordinates.0.longitude)
        let location2 = CLLocation(latitude: coordinates.1.latitude, longitude: coordinates.1.longitude)
        return location1.distance(from: location2)
    }
}
