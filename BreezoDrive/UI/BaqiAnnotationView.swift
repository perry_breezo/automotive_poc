//
//  BaqiAnnotationView.swift
//  SomeApp
//
//  Created by Perry on 01/04/2016.
//  Copyright © 2016 PerrchicK. All rights reserved.
//

import Foundation
import MapKit

class BaqiAnnotationView: UIView {
    static let AnnotationIdentifier: String = "BaqiAnnotationView"
    @IBOutlet weak var annotationIconLabel: UILabel!
    @IBOutlet weak var annotationIconLabelBackground: UIView!

    override func awakeFromNib() {
        //annotationIconLabel.font = annotationIconLabel.font.withSize(20)
        annotationIconLabel.makeRoundedCorners(20)
        annotationIconLabel.backgroundColor = UIColor.white
        annotationIconLabelBackground.makeRoundedCorners(25)
        isUserInteractionEnabled = false
    }
    
    var bgColor: UIColor? {
        set {
            annotationIconLabelBackground.backgroundColor = newValue
        }
        get {
            return annotationIconLabelBackground.backgroundColor
        }
    }
    
    var text: String? {
        set {
            annotationIconLabel.text = newValue
        }
        get {
            return annotationIconLabel.text
        }
    }

//    func generateIcon() -> String {
//        let randomIndex = 0 ~ possibleIcons.count
//        return possibleIcons[randomIndex]
//    }
}
