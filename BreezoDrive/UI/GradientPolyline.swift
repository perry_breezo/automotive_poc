//
//  GradientPolyline.swift
//  Runner
//
//  Created by Perry Shalom on 06/01/2019.
//  Copyright © 2019 The Chromium Authors. All rights reserved.
//

import MapKit
import UIKit

/// Inspired from: https://stackoverflow.com/questions/5682688/gradient-polyline-with-mapkit-ios
class GradientPolylineOverlay: MKPolyline {
    var baqis: [Int]?
    public func getColor(from index: Int) -> CGColor {
        return UIColor(withBaqi: baqis![safe: index]!).cgColor
    }
}

extension GradientPolylineOverlay {
    convenience init(qualities: [AirQualityData]) {
        let coordinates: [CLLocationCoordinate2D] = qualities.map( { $0.locationCoordinate.toLocationCoordinate2D() } )
        self.init(coordinates: coordinates, count: coordinates.count)
        
        baqis = qualities.map({ 📗($0.baqi); return $0.baqi })
    }

    func renderer(withLineWidth lineWidth: CGFloat = 7) -> GradientPolylineRenderer {
        let polyLineRender = GradientPolylineRenderer(polyline: self)
        polyLineRender.lineWidth = lineWidth
        return polyLineRender
    }
}

class GradientPolylineRenderer: MKPolylineRenderer {
    
    override func draw(_ mapRect: MKMapRect, zoomScale: MKZoomScale, in context: CGContext) {
        let boundingBox = self.path.boundingBox
        let mapRectCG = rect(for: mapRect)
        
        if(!mapRectCG.intersects(boundingBox)) { return }
        
        
        var previousColor: CGColor?
        var currentColor: CGColor?
        
        guard let polyLine = self.polyline as? GradientPolylineOverlay else { return }
        
        for index in 0...self.polyline.pointCount - 1{
            let point = self.point(for: self.polyline.points()[index])
            let path = CGMutablePath()
            
            currentColor = polyLine.getColor(from: index)
            
            if index == 0 {
                path.move(to: point)
            } else {
                let previousPoint = self.point(for: self.polyline.points()[index - 1])
                path.move(to: previousPoint)
                path.addLine(to: point)
                
                let colors = [previousColor!, currentColor!] as CFArray
                let baseWidth = self.lineWidth / zoomScale
                
                context.saveGState()
                context.addPath(path)
                
                let gradient = CGGradient(colorsSpace: nil, colors: colors, locations: [0, 1])
                
                context.setLineWidth(baseWidth)
                context.replacePathWithStrokedPath()
                context.clip()
                context.drawLinearGradient(gradient!, start: previousPoint, end: point, options: [])
                context.restoreGState()
            }

            previousColor = currentColor
        }
    }
}
