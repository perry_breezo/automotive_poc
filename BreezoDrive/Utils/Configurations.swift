//
//  Configurations.swift
//  SomeApp
//
//  Created by Perry on 2/13/16.
//  Copyright © 2016 PerrchicK. All rights reserved.
//

import Foundation
//import FirebaseRemoteConfig

class Configurations {
    static let shared = Configurations()

    struct Constants {
        static let ClosestZoomRatioScale: Double = 591657550.50
        static let BreezoApiKey: String = "5d3d499fc6ac48d187bb0e5a6309eb6e" // "68fccdf466ac4aa6b8c467ea93870d9f"
    }

    struct Keys {
        struct RemoteConfig {
            static let MaximumParkLifeInMinutes: String = "MaximumParkLifeInMinutes"
        }
        
        struct Persistency {
            static let PresentedJsonFileName: String    = "temp-file-to-show.js"
            static let PermissionRequestCounter: String = "PermissionRequestCounter"
            static let LastCrash: String                = "last crash"
        }
    }

    let GoogleMapsWebApiKey: String
    let GoogleMapsMobileSdkApiKey: String

    lazy var currentVersion: String = {
        let infoPlist: [String : Any]? = NSDictionary(contentsOfFile: Bundle.main.path(forResource: "Info", ofType: "plist")!) as? [String : Any]
        let selectedVersion: String = (infoPlist?["app_version"] as? String) ?? "test_production"
        return selectedVersion
    }()

    private(set) var shouldMockLocationAndData: Bool
    private(set) var shouldShowBaqiMarkers: Bool
    private(set) var shouldShowUserLocation: Bool
    private(set) var shouldUseChinaEndPoint: Bool
    
    //private var remoteConfig: RemoteConfig

    private init() {
        //remoteConfig = RemoteConfig.remoteConfig()

        shouldMockLocationAndData = PerrFuncs.isRunningOnSimulator()
        shouldShowBaqiMarkers = true
        shouldShowUserLocation = !shouldMockLocationAndData //true
        shouldUseChinaEndPoint = true

//        if let remoteConfigSettings = RemoteConfigSettings(developerModeEnabled: true) {
//            remoteConfig.configSettings = remoteConfigSettings
//        }

        guard let secretConstantsPlistFilePath: String = Bundle.main.path(forResource: "SecretConstants", ofType: "plist"),
        let config: [String:String] = NSDictionary(contentsOfFile: secretConstantsPlistFilePath) as? [String:String],
        let googleMapsWebApiKey = config["GoogleMapsWebApiKey"],
        let googleMapsMobileSdkApiKey = config["GoogleMapsMobileSdkApiKey"]
        else { fatalError("No way! The app must have this plist file with the mandatory keys") }

        GoogleMapsWebApiKey = googleMapsWebApiKey
        GoogleMapsMobileSdkApiKey = googleMapsMobileSdkApiKey
    }
//    func fetchRemoteConfig() {
//        remoteConfig.fetch(withExpirationDuration: 2, completionHandler: { [weak self] (fetchStatus, error) -> () in
//            if fetchStatus == .success {
//                if let maximumParkLifeInMinutes = self?.remoteConfig[Keys.RemoteConfig.MaximumParkLifeInMinutes].numberValue?.intValue, maximumParkLifeInMinutes > 0 {
//                    self?.maximumParkLifeInMinutes = maximumParkLifeInMinutes
//                }
//            } else if let error = error {
//                📕("Failed to fetch remote config! Error: \(error)")
//            } else {
//                📕("Failed to fetch remote config! Missing error object...")
//            }
//        })
//    }
}
