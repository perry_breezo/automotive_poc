//
//  LocationHelper.swift
//  JobInterviewHW2.0
//
//  Created by Perry on 01/12/2017.
//  Copyright © 2017 perrchick. All rights reserved.
//

import Foundation
import CoreLocation

struct LocationCoordinate: Codable {
    var latitude: Double
    var longitude: Double
    
    init(latitude: Double, longitude: Double) {
        self.latitude = latitude
        self.longitude = longitude
    }
}

typealias city_address_tuple = (cityName: String, streetName: String)

protocol LocationHelperDelegate: class {
    func onLocationUpdated(updatedLocation: CLLocation)
}

class LocationHelper: NSObject, CLLocationManagerDelegate {
    static let shared: LocationHelper = LocationHelper()

    weak var delegate: LocationHelperDelegate?
    let geocoder: CLGeocoder
    private(set) var currentLocation: CLLocation?
    func currentAddress(completion: @escaping CallbackClosure<city_address_tuple?>) {
        guard let currentLocation = currentLocation else { completion(nil); return }

        geocoder.reverseGeocodeLocation(currentLocation, completionHandler: { (placemark, error) in
            guard let placemark = placemark else { completion(nil); return }
            // https://developer.apple.com/documentation/corelocation/clplacemark/1423814-thoroughfare
            let streetName = placemark.first?.thoroughfare ?? "unknown_street".localized()
            let cityName = placemark.first?.locality ?? "unknown_city".localized()
            completion((cityName, streetName))
        })
    }
    lazy var locationManager: CLLocationManager = {
        let locationManager: CLLocationManager = CLLocationManager();
        locationManager.delegate = self
        return locationManager
    }()

    override init() {
        geocoder = CLGeocoder()
        super.init()

        NotificationCenter.default.addObserver(self, selector: #selector(applicationDidEnterBackground), name: UIApplication.didEnterBackgroundNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(applicationWillEnterForeground), name: UIApplication.willEnterForegroundNotification, object: nil)

        locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation
        locationManager.activityType = .automotiveNavigation
    }

    var distanceFilter: CLLocationDistance {
        get {
            return locationManager.distanceFilter
        }
        set {
            locationManager.distanceFilter = newValue
        }
    }

    func startUpdate() {
        guard isPermissionGranted else { requestPermissionsIfNeeded(); return }
        locationManager.startUpdatingLocation()
        locationManager.allowsBackgroundLocationUpdates = true
    }

    func stopUpdate() {
        locationManager.stopUpdatingLocation()
        //locationManager.allowsBackgroundLocationUpdates = false
    }

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.first {
            currentLocation = location
            delegate?.onLocationUpdated(updatedLocation: location)
        }
    }

    var isPermissionGranted: Bool {
        return CLLocationManager.authorizationStatus() == .authorizedWhenInUse
    }

    @objc func applicationWillEnterForeground(notification: Notification) {
        startUpdate()
    }

    @objc func applicationDidEnterBackground(notification: Notification) {
        stopUpdate()
    }
    
    var isCarSpeed: Bool {
        return (currentLocation?.speed).or(0) > 5
    }

    var isAlmostIdle: Bool {
        return (currentLocation?.speed).or(0) < 1
    }

    func requestPermissionsIfNeeded() {
        let counterKey: String = Configurations.Keys.Persistency.PermissionRequestCounter;
        let permissionRequestCounter: Int = UserDefaults.load(key: counterKey, defaultValue: 0)
        if permissionRequestCounter == 0 || CLLocationManager.authorizationStatus() == .notDetermined {
            // First time for this life time
            locationManager.requestWhenInUseAuthorization()
        } else {
            if let settingsUrl = URL(string: UIApplication.openSettingsURLString) {
                if UIApplication.shared.canOpenURL(settingsUrl) {
                    if #available(iOS 10.0, *) {
                        UIApplication.shared.open(settingsUrl, completionHandler: nil)
                    } else {
                        // Fallback on earlier versions
                        UIApplication.shared.openURL(settingsUrl)
                    }
                }
            }
        }

        UserDefaults.save(value: permissionRequestCounter + 1, forKey: Configurations.Keys.Persistency.PermissionRequestCounter).synchronize()
    }

    private static func parseGeocodeResponse(_ responseObject: Any) -> String? {
        var result: String?
        
        guard let responseDictionary = responseObject as? RawJsonFormat,
            let status = responseDictionary["status"] as? String, status == "OK" else { return result }
        
        if let results = responseDictionary["results"] as? [AnyObject],
            let firstPlace = results[0] as? [String:AnyObject],
            let firstPlaceName = firstPlace["formatted_address"] as? String {
            result = firstPlaceName
        }
        
        return result
    }
}
