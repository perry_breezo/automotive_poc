//
//  DataManager.swift
//  SomeApp
//
//  Created by Perry on 3/17/16.
//  Copyright © 2016 PerrchicK. All rights reserved.
//

import Foundation
import CoreData
import UIKit

class DataManager {
    static let shared = DataManager()
    var current_city_street_tuple: city_address_tuple?
    
    private lazy var responses: [String] = []

    private init() {
        //responses = [:]//[NSNumber:[NSObject:NSObject]] = [:]
    }

    /// /var/mobile/Containers/Data/Application/<APP_GUID>/Documents/Videos/...
    lazy var recordsDirectory: NSString = {
        let directory: String = self.documentsDirectory.appendingPathComponent("Records")
        if DataManager.doesItemExistAtPath(directory) == false {
            do {
                try FileManager.default.createDirectory(atPath: directory, withIntermediateDirectories: true, attributes: nil)
            } catch let error {
                📕("Failed to find caches directory, error: \(error)")
            }
        }
        
        return directory as NSString
    }()

    lazy var viewerDirectory: NSString = {
        let directory: String = self.documentsDirectory.appendingPathComponent("Viewer")
        if DataManager.doesItemExistAtPath(directory) == false {
            do {
                try FileManager.default.createDirectory(atPath: directory, withIntermediateDirectories: true, attributes: nil)
            } catch let error {
                📕("Failed to find caches directory, error: \(error)")
            }
        }
        
        return directory as NSString
    }()

    lazy var documentsDirectory: NSString = {
        //        let documentsDirectoryUrl = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        if let documentsDirectory = paths.first {
            return documentsDirectory as NSString
        }
        
        📕("Failed to find documents directory")
        return ""
    }()
    
    lazy var libraryDirectory: NSString = {
        //        let documentsDirectoryUrl = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first
        let paths = NSSearchPathForDirectoriesInDomains(.libraryDirectory, .userDomainMask, true)
        if let documentsDirectory = paths.first {
            return documentsDirectory as NSString
        }
        
        📕("Failed to find documents directory")
        return ""
    }()
    
    lazy var cachesDirectory: NSString = {
        let paths = NSSearchPathForDirectoriesInDomains(.cachesDirectory, .userDomainMask, true)
        if let cachesDirectory = paths.first {
            return cachesDirectory as NSString
        }
        
        📕("Failed to find caches directory")
        return ""
    }()
    
    static var applicationLibraryPath: NSString = {
        if let libraryDirectoryPath = NSSearchPathForDirectoriesInDomains(.libraryDirectory, .userDomainMask, true).last {
            return libraryDirectoryPath as NSString
        }
        
        📕("ERROR!! Library directory not found 😱")
        return ""
    }()

    static var applicationDocumentsPath: URL = {
        guard let documentsDirectory = FileManager().urls(for: .documentDirectory, in: .userDomainMask).first else {
            📕("ERROR!! Library directory not found 😱")
            return URL(fileURLWithPath: "")
        }

        return documentsDirectory
    }()

    fileprivate static var managedContext: NSManagedObjectContext {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        return appDelegate.managedObjectContext
    }

    @discardableResult
    static func saveImage(_ imageToSave: UIImage, toFile filename: String) -> Bool {
        if let data = imageToSave.pngData() {
            do {
                try data.write(to: URL(fileURLWithPath: (applicationLibraryPath as String) + "/" + filename), options: .atomicWrite)
                return true
            } catch {
                📕("Failed to save image!")
            }
        }

        return false
    }

    /// This method blocks the current thread and downloads the image from this URL string in case the URL is valid
    static func downloadImage(fromUrl imageUrlString: String) -> UIImage? {
        if let imageUrl = URL(string: imageUrlString), let imageData = try? Data(contentsOf: imageUrl) {
            return UIImage(data: imageData)
        }
        
        return nil
    }
    
    static func loadImage(fromFile filename: String) -> UIImage? {
        if let data = try? Data(contentsOf: URL(fileURLWithPath: (applicationLibraryPath as String) + "/" + filename)) {
            return UIImage(data: data)
        }

        return nil
    }

//    static func saveJson(withConditions conditions: [AirQualityData]) -> String {
//        let _objectData: Data? = try? JSONEncoder().encode(conditions)
//        guard let objectData = _objectData else { return "" }
//        if let jsonString = String(data: objectData, encoding: .utf8) {
//            return jsonString
//        } else {
//            📕("Failed to serizlize JSON from conditions array: \(conditions)")
//            return ""
//        }
//    }
    
    static func fetchBaqi(latitude: Double, longitude: Double, completion: @escaping CallbackClosure<AirQualityData?>) {
        Communicator.fetchBaqi(latitude: latitude, longitude: longitude, completion: { json in
            guard let json = json, let aqData: AirQualityData = AirQualityData.fromDictionary(objectDictionary: json) else { completion(nil); return }
            
            📗("Parsed response data into model: \(aqData)")
            completion(aqData)
        })
    }
    
    func startSavingResponses() {
        //responses = [:]
        responses.removeAll()
    }
    
    func addResponse(json: RawJsonFormat) {
        let _jsonData = try? JSONSerialization.data(withJSONObject: json, options: JSONSerialization.WritingOptions.prettyPrinted)
        guard let jsonData = _jsonData else { return }
        guard let jsonString = String(data: jsonData, encoding: .utf8) else { return }
        if responses.count == 0 {
            LocationHelper.shared.currentAddress { (city_street_tuple) in
                guard let city_street_tuple = city_street_tuple else { 📕("failed to fetch current address!"); return }
                self.current_city_street_tuple = city_street_tuple
            }
        }
        responses.append(jsonString)
    }

    var resultsJson: String? {
        do {
            let _objectData: Data = try JSONEncoder().encode(["results":responses])
            
            // try! JSONSerialization.jsonObject(with: String(data: try! JSONEncoder().encode(responses as [String:String]), encoding: .utf8)!.data(using: .utf8)!, options : .allowFragments) as? [String:Any]
            
            // responses
            // try! JSONSerialization.jsonObject(with: String(data: try! JSONEncoder().encode(["results":responses]), encoding: .utf8)!.data(using: .utf8)!, options : .allowFragments) as? [String:Any]
            
            //
//AnyObject

            //JSONSerialization.strin
            let jsonObject = try! JSONSerialization.jsonObject(with: String(data: try! JSONEncoder().encode(["results":responses]), encoding: .utf8)!.data(using: .utf8)!, options : .allowFragments) as? RawJsonFormat

            //let jData = try! JSONEncoder().encode(try! JSONSerialization.jsonObject(with: String(data: try! JSONEncoder().encode(["results":responses]), encoding: .utf8)!.data(using: .utf8)!, options : .allowFragments) as! [String:Any])
            return String(data: _objectData, encoding: .utf8)
        } catch {
            📕("Failed to serizlize JSON from conditions array: \(responses). Error: \(error)")
        }

        return nil
    }
    
    func saveResponsesJson(completion: @escaping CallbackClosure<String?>) {
        if responses.count > 0, let _resultsJson = resultsJson {
            let cityName1 = current_city_street_tuple?.cityName ?? "unknown_street".localized()
            let streetName1 = current_city_street_tuple?.streetName ?? "unknown_city".localized()
            LocationHelper.shared.currentAddress { (city_street_tuple) in
                let cityName2 = city_street_tuple?.cityName ?? "unknown_street".localized()
                let streetName2 = city_street_tuple?.streetName ?? "unknown_city".localized()
                let fileName = "\(Timestamp.current)_\(streetName1)_in_\(cityName1)-\(streetName2)_in_\(cityName2).js"

                PerrFuncs.runOnBackground {
                    let filePath = self.recordsDirectory.appendingPathComponent(fileName)
                    do {
                        try _resultsJson.write(toFile: filePath, atomically: true, encoding: String.Encoding.utf8)
                        completion(filePath)
                    } catch {
                        📕("Failed to save records! Error: \(error)")
                        completion(nil)
                    }
//                    if !NSKeyedArchiver.archiveRootObject(_responses, toFile: savedRecords) {
//                        📕("Failed to save records!")
//                    } else {
//
//                    }
                }
            }

            self.current_city_street_tuple = nil
        } else {
            completion(nil)
        }
    }
    
    static func share(fileWithUrlString filePath: String) {
        //let localFileUrl = URL(fileURLWithPath: filePath)
        guard let localFileUrl = URL(string: filePath) else { return }
        share(fileWithUrl: localFileUrl)
    }

    static func share(fileWithUrl localFileUrl: URL) {
        let activityViewController = UIActivityViewController(activityItems: [localFileUrl] , applicationActivities: nil)
        
        guard let mostTopViewController = UIApplication.mostTopViewController() else { return }
        if let popOver = activityViewController.popoverPresentationController {
            // We're running on an iPad
            popOver.sourceView = mostTopViewController.view
            //popOver.sourceRect =
            //popOver.barButtonItem
        }
        mostTopViewController.present(activityViewController, animated: true, completion: nil)
    }
    
    func savedRecordsList() -> [URL] {
        var filesList = [URL]()
        guard let recordsDirectoryUrl = URL(string: self.recordsDirectory as String) else { return filesList }
        
        do {
            filesList = try FileManager.default.contentsOfDirectory(at: recordsDirectoryUrl, includingPropertiesForKeys: nil, options: [])
        } catch let error {
            📕("Failed to load cached video files list, error \(error)")
        }

        return filesList.filter( { !$0.lastPathComponent.contains("temp-file-to-show.js") && !$0.lastPathComponent.contains("json-viewer.html") } )
    }

    func prepareJsonViewer(forJsonFileWithPath jsonFileUrl: URL) -> URL? {
        guard let bundlePath = Bundle.main.path(forResource: "json-viewer", ofType: "html") else { return nil }

        let fileManager = FileManager.default
        var destinationPath: String
        destinationPath = self.viewerDirectory.appendingPathComponent(Configurations.Keys.Persistency.PresentedJsonFileName)
        
        do {
            if DataManager.doesItemExistAtPath(destinationPath) {
                try fileManager.removeItem(atPath: destinationPath)
            }
            let fileContent = try String(contentsOf: jsonFileUrl)
            let readableJsonSnippet = "var resultsJson = " + fileContent
            try readableJsonSnippet.write(toFile: destinationPath, atomically: true, encoding: String.Encoding.utf8)
            //try fileManager.copyItem(atPath: jsonFilePath.removeFileSchemePrefix(), toPath: )
        } catch {
            📕(error)
            return nil
        }

        destinationPath = self.viewerDirectory.appendingPathComponent("json-viewer.html")
        if !DataManager.doesItemExistAtPath(destinationPath) {
            do {
                try fileManager.copyItem(atPath: bundlePath, toPath: destinationPath)
            } catch {
                📕(error)
                return nil
            }
        }

        return destinationPath.toUrlFilePath()
    }

    func deleteAllRecordsHostory() {
        let filesList = savedRecordsList()
        for localVideoFileUrl in filesList {
            DataManager.deleteFile(atUrl: localVideoFileUrl)
        }
    }

    //MARK: - File system methods
    
    @discardableResult
    static func deleteFileAtPath(_ filePath: String) -> Bool {
        var deletedSuccessFully = false
        
        do {
            try FileManager.default.removeItem(atPath: filePath)
            deletedSuccessFully = true
        } catch let error {
            📕("Failed to delete file from path: '\(filePath)'. Error: \(error)")
        }
        
        return deletedSuccessFully
    }
    
    @discardableResult
    static func deleteFile(atUrl filePathUrl: URL) -> Bool {
        //guard DataManager.doesItemExistAtUrl(filePathUrl) else { return false }
        var deletedSuccessFully = false
        
        do {
            try FileManager.default.removeItem(at: filePathUrl)
            deletedSuccessFully = true
        } catch let error {
            📕("Failed to delete file from URL path: '\(filePathUrl.absoluteString)', ARE YOU TRYING TO DELETE FROM DOCUMENTS FOLDER?? TRY DO USE LIBRARY INSTEAD! Error: \(error)")
        }
        
        return deletedSuccessFully
    }

    /**
     Returns a Boolean value that indicates whether a file or directory exists at a specified path.
     */
    static func doesItemExistAtPath(_ filePath: String) -> Bool {
        let pathToCheck = filePath.replacingOccurrences(of: "file:///", with: "")
        let doesExist = FileManager.default.fileExists(atPath: pathToCheck)
        return doesExist
    }
    
    static func doesItemExistAtUrl(_ filePathUrl: URL) -> Bool {
        return doesItemExistAtPath(filePathUrl.absoluteString)
    }
}

extension Timestamp {
    static var current: Timestamp {
        return Date().timestamp
    }
    
}
